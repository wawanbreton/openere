QT += core gui sql xml multimedia
CONFIG += c++1z

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OpenERE
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

QMAKE_CXXFLAGS += -Wsuggest-override

SOURCES += \
        main.cpp \
    gui/scenario/scenarioeditorwidget.cpp \
    gui/scenario/scenariodeviceswidget.cpp \
    models/scenario.cpp \
    models/device.cpp \
    commands/scenario/abstractscenarioeditorcommand.cpp \
    commands/scenario/addscenariodevicecommand.cpp \
    gui/scenario/scenariodevicestablemodel.cpp \
    gui/delegates/comboboxitemdelegate.cpp \
    commands/generic/setobjectpropertycommand.cpp \
    commands/scenario/removescenariodevicecommand.cpp \
    database/databaseaccessor.cpp \
    models/medium.cpp \
    gui/scenario/scenariomediawidget.cpp \
    gui/scenario/scenariomediatablemodel.cpp \
    commands/scenario/addscenariomediumcommand.cpp \
    commands/scenario/removescenariomediumcommand.cpp \
    utils/parser.cpp \
    utils/xml.cpp \
    utils/debug.cpp

HEADERS += \
    gui/scenario/scenarioeditorwidget.h \
    gui/scenario/scenariodeviceswidget.h \
    models/scenario.h \
    models/devicetype.h \
    models/device.h \
    commands/scenario/abstractscenarioeditorcommand.h \
    commands/scenario/addscenariodevicecommand.h \
    gui/scenario/scenariodevicestablemodel.h \
    gui/delegates/comboboxitemdelegate.h \
    commands/generic/setobjectpropertycommand.h \
    commands/scenario/removescenariodevicecommand.h \
    database/databaseaccessor.h \
    utils/enum.h \
    models/medium.h \
    models/mediumtype.h \
    gui/scenario/scenariomediawidget.h \
    gui/scenario/scenariomediatablemodel.h \
    commands/scenario/addscenariomediumcommand.h \
    commands/scenario/removescenariomediumcommand.h \
    utils/parser.h \
    utils/xml.h \
    utils/debug.h

FORMS += \
    gui/scenario/scenarioeditorwidget.ui \
    gui/scenario/scenariodeviceswidget.ui \
    gui/scenario/scenariomediawidget.ui

RESOURCES += \
    assets.qrc
