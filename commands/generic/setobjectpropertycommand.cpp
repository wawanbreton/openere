#include "setobjectpropertycommand.h"


SetObjectPropertyCommand::SetObjectPropertyCommand(QObject *object,
                                                   const QString &propertyName,
                                                   const QVariant &value) :
    QUndoCommand(),
    _object(object),
    _propertyName(propertyName),
    _newValue(value),
    _oldValue(object->property(propertyName.toStdString().c_str()))
{
}

void SetObjectPropertyCommand::redo()
{
    _object->setProperty(_propertyName.toStdString().c_str(), _newValue);
}

void SetObjectPropertyCommand::undo()
{
    _object->setProperty(_propertyName.toStdString().c_str(), _oldValue);
}
