#pragma once

#include <QUndoCommand>

#include <QVariant>

class SetObjectPropertyCommand : public QUndoCommand
{
    public:
        SetObjectPropertyCommand(QObject *object,
                                 const QString &propertyName,
                                 const QVariant &value);

    public:
        virtual void redo() override;

        virtual void undo() override;

    private:
        QObject *_object{nullptr};
        const QString _propertyName;
        const QVariant _newValue;
        const QVariant _oldValue;
};
