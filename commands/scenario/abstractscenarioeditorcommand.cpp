#include "abstractscenarioeditorcommand.h"


AbstractScenarioEditorCommand::AbstractScenarioEditorCommand(Scenario *scenario) :
    QUndoCommand(),
    _scenario(scenario)
{
}
