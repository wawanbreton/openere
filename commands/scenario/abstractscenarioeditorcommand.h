#pragma once

#include <QUndoCommand>

class Scenario;

class AbstractScenarioEditorCommand : public QUndoCommand
{
    public:
        explicit AbstractScenarioEditorCommand(Scenario *scenario);

    protected:
        Scenario *accessScenario() { return _scenario; }

    private:
        Scenario *_scenario{nullptr};
};
