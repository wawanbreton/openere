#include "addscenariodevicecommand.h"

#include <QDebug>

#include "models/device.h"
#include "models/scenario.h"


AddScenarioDeviceCommand::AddScenarioDeviceCommand(Scenario *scenario) :
    QObject(),
    AbstractScenarioEditorCommand(scenario),
    _device(new Device(this))
{
}

void AddScenarioDeviceCommand::redo()
{
    _device->setName(tr("Nouveau périphérique"));
    accessScenario()->addDevice(_device);
}

void AddScenarioDeviceCommand::undo()
{
    accessScenario()->removeDevice(_device);
    _device->setParent(this);
}
