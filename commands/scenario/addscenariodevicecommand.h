#pragma once

#include <QObject>
#include "commands/scenario/abstractscenarioeditorcommand.h"

#include <QCoreApplication>

class Device;

class AddScenarioDeviceCommand : public QObject, public AbstractScenarioEditorCommand
{
    Q_OBJECT

    public:
        explicit AddScenarioDeviceCommand(Scenario *scenario);

        virtual void redo() override;

        virtual void undo() override;

    private:
        Device *_device{nullptr};
};
