#include "addscenariomediumcommand.h"

#include <QDebug>
#include <QFileInfo>
#include <QMessageBox>
#include <QMimeDatabase>

#include "models/medium.h"
#include "models/scenario.h"


AddScenarioMediumCommand::AddScenarioMediumCommand(const QStringList &filesPath,
                                                   Scenario *scenario) :
    QObject(),
    AbstractScenarioEditorCommand(scenario)
{
    QMimeDatabase mimeDb;
    QStringList unrecognizedFiles;

    for(const QString &filePath : filesPath)
    {
        QFileInfo fileInfo(filePath);
        Medium *medium = new Medium(this);

        bool typeRecognized = false;
        QMimeType fullType = mimeDb.mimeTypeForFile(fileInfo);
        QStringList mimeNameParts = fullType.name().split('/');
        if(mimeNameParts.count() == 2)
        {
            QString type = mimeNameParts.first();
            if(type == "image")
            {
                medium->setType(MediumType::Picture);
                typeRecognized = true;
            }
            else if(type == "video")
            {
                medium->setType(MediumType::Video);
                typeRecognized = true;
            }
            else if(type == "audio")
            {
                medium->setType(MediumType::Sound);
                typeRecognized = true;
            }
        }

        if(typeRecognized)
        {
            _media.append({medium, fileInfo.baseName()});
        }
        else
        {
            delete medium;
            unrecognizedFiles.append(fileInfo.fileName());
        }
    }

    if(!unrecognizedFiles.isEmpty())
    {
        QMessageBox *messageBox = new QMessageBox();
        messageBox->setIcon(QMessageBox::Critical);

        QString message(tr("Fichiers non supportés :"));
        message.append("\n\n");
        message.append(unrecognizedFiles.join('\n'));
        messageBox->setText(message);

        connect(messageBox, &QMessageBox::finished, messageBox, &QMessageBox::deleteLater);

        messageBox->show();
    }

    setObsolete(_media.isEmpty());
}

void AddScenarioMediumCommand::redo()
{
    for(const QPair<Medium *, QString> &medium : _media)
    {
        medium.first->setName(medium.second);
        accessScenario()->addMedium(medium.first);
    }
}

void AddScenarioMediumCommand::undo()
{
    for(const QPair<Medium *, QString> &medium : _media)
    {
        accessScenario()->removeMedium(medium.first);
        medium.first->setParent(this);
    }
}
