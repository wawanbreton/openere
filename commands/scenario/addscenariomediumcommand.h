#pragma once

#include <QObject>
#include "commands/scenario/abstractscenarioeditorcommand.h"

#include <QCoreApplication>

class Medium;

class AddScenarioMediumCommand : public QObject, public AbstractScenarioEditorCommand
{
    Q_OBJECT

    public:
        explicit AddScenarioMediumCommand(const QStringList &filesPath, Scenario *scenario);

        virtual void redo() override;

        virtual void undo() override;

    private:
        QList<QPair<Medium *, QString>> _media;
};
