#include "removescenariodevicecommand.h"

#include <QDebug>

#include "models/device.h"
#include "models/scenario.h"


RemoveScenarioDeviceCommand::RemoveScenarioDeviceCommand(Scenario *scenario, Device *device) :
    AbstractScenarioEditorCommand(scenario),
    _device(device),
    _index(scenario->getDevices().indexOf(device))
{
}

void RemoveScenarioDeviceCommand::redo()
{
    accessScenario()->removeDevice(_device);
    _device->setParent(this);
}

void RemoveScenarioDeviceCommand::undo()
{
    accessScenario()->addDevice(_device, _index);
}
