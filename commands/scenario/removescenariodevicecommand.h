#pragma once

#include <QObject>
#include "commands/scenario/abstractscenarioeditorcommand.h"

#include <QCoreApplication>

class Device;

class RemoveScenarioDeviceCommand : public QObject, public AbstractScenarioEditorCommand
{
    Q_OBJECT

    public:
        explicit RemoveScenarioDeviceCommand(Scenario *scenario, Device *device);

        virtual void redo() override;

        virtual void undo() override;

    private:
        Device *_device{nullptr};
        int _index{-1};
};
