#include "removescenariomediumcommand.h"

#include <QDebug>

#include "models/medium.h"
#include "models/scenario.h"


RemoveScenarioMediumCommand::RemoveScenarioMediumCommand(Scenario *scenario, Medium *medium) :
    AbstractScenarioEditorCommand(scenario),
    _medium(medium),
    _index(scenario->getMedia().indexOf(medium))
{
}

void RemoveScenarioMediumCommand::redo()
{
    accessScenario()->removeMedium(_medium);
    _medium->setParent(this);
}

void RemoveScenarioMediumCommand::undo()
{
    accessScenario()->addMedium(_medium, _index);
}
