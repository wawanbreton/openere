#pragma once

#include <QObject>
#include "commands/scenario/abstractscenarioeditorcommand.h"

#include <QCoreApplication>

class Medium;

class RemoveScenarioMediumCommand : public QObject, public AbstractScenarioEditorCommand
{
    Q_OBJECT

    public:
        explicit RemoveScenarioMediumCommand(Scenario *scenario, Medium *medium);

        virtual void redo() override;

        virtual void undo() override;

    private:
        Medium *_medium{nullptr};
        int _index{-1};
};
