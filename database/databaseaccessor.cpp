#include "databaseaccessor.h"

#include <QDebug>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QSqlError>


QSqlDatabase DatabaseAccessor::_database;

void DatabaseAccessor::init()
{
    _database = QSqlDatabase::addDatabase("QPSQL", "OpenERE");
    _database.setHostName("127.0.0.1");
    _database.setUserName("OpenERE");
    _database.setPassword("IwTeW1h!");
    _database.setDatabaseName("OpenERE");
    _database.setPort(5432);

    if(_database.open())
    {
        qInfo() << "Successfully connected to database";

        int actualVersion = -1;
        QSqlQuery query = _database.exec("SELECT version FROM version");
        QSqlError error = query.lastError();
        if(error.isValid())
        {
            if(error.nativeErrorCode() == "42P01")
            {
                qInfo() << "Database seems to be empty";
                actualVersion = 0;
            }
            else
            {
                qWarning() << "Unknown database error" << error.databaseText();
            }
        }
        else if(query.next())
        {
            actualVersion = query.value(0).toInt();
            qInfo() << "Database actual version is" << actualVersion;
        }
        else
        {
            qWarning() << "Versions table exists but it is empty ?!";
            actualVersion = 0;
        }

        if(actualVersion >= 0)
        {
            actualVersion++;
            QString request;
            bool requestError = false;
            do
            {
                request = updateToVersionRequest(actualVersion);
                if(!request.isEmpty())
                {
                    qInfo() << "Updating to version" << actualVersion;
                    _database.driver()->beginTransaction();

                    _database.exec(request);

                    QSqlQuery querySetVersion(_database);
                    querySetVersion.prepare("UPDATE version SET version=:version");
                    querySetVersion.bindValue(":version", actualVersion);
                    querySetVersion.exec();

                    requestError = !_database.driver()->commitTransaction();
                    if(!requestError)
                    {
                        qInfo() << "Successfully updated to version" << actualVersion;
                    }
                    else
                    {
                        qWarning() << "Error when updating to version" << actualVersion << ":"
                                   << _database.lastError().text();
                    }
                }
                actualVersion++;
            } while(!request.isEmpty() && !requestError);
        }
    }
    else
    {
        qWarning() << "Unable to connect to database :" << _database.lastError().text();
    }
}

QString DatabaseAccessor::updateToVersionRequest(int version)
{
    if(version == 1)
    {
        // Just create the versions table
        return "CREATE TABLE version (version integer);"
               "INSERT INTO version(version) VALUES (0);";
    }
    else if(version == 2)
    {
        // Create basic tables for scenarios
        return "CREATE TABLE scenarii (id integer,"
               "                       name text,"
               "                       PRIMARY KEY (id));"

               "CREATE TABLE scenarii_versions (id integer,"
               "                                id_scenario integer,"
               "                                id_parent_version integer,"
               "                                version integer,"
               "                                data text,"
               "                                PRIMARY KEY (id),"
               "                                FOREIGN KEY (id_scenario) REFERENCES scenarii(id))"
               "                                FOREIGN KEY (id_parent_version) REFERENCES scenarii_versions(id));"

               "CREATE TABLE media (id integer,"
               "                    type integer,"
               "                    name text,"
               "                    data bytea,"
               "                    checksum varchar(32)"
               "                    PRIMARY KEY (id));"

               "CREATE TABLE scenarii_media (id integer,"
               "                             id_scenario_version integer,"
               "                             id_medium integer,"
               "                             PRIMARY KEY (id))"
               "                             FOREIGN KEY (id_scenario_version) REFERENCES scenarii_versions(id))"
               "                             FOREIGN KEY (id_medium) REFERENCES media(id));";
    }

    return QString();
}
