#pragma once

#include <QSqlDatabase>

class DatabaseAccessor
{
    public:
        static void init();

        static QSqlDatabase &db() { return _database; }

    private:
        static QString updateToVersionRequest(int version);

    private:
        static QSqlDatabase _database;
};
