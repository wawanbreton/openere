#include "comboboxitemdelegate.h"

#include <QComboBox>
#include <QDebug>


ComboBoxItemDelegate::ComboBoxItemDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

QWidget *ComboBoxItemDelegate::createEditor(QWidget *parent,
                                            const QStyleOptionViewItem &option,
                                            const QModelIndex &index) const
{
    Q_UNUSED(option)
    Q_UNUSED(index)

    QComboBox *comboBox = new QComboBox(parent);
    comboBox->setIconSize(_iconSize);

    for(const ComboBoxEntry &entry : _comboBoxData)
    {
        comboBox->addItem(entry.icon, entry.text, entry.data);
    }

    return comboBox;
}


void ComboBoxItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QComboBox *comboBox = qobject_cast<QComboBox *>(editor);
    if(comboBox)
    {
        comboBox->setCurrentIndex(comboBox->findData(index.data(Qt::EditRole)));
    }
    else
    {
        qCritical() << "Editor is not a QComboBox" << editor;
    }
}


void ComboBoxItemDelegate::setModelData(QWidget *editor,
                                        QAbstractItemModel *model,
                                        const QModelIndex &index) const
{
    QComboBox *comboBox = qobject_cast<QComboBox *>(editor);
    if(comboBox)
    {
        model->setData(index, comboBox->currentData(), Qt::EditRole);
    }
    else
    {
        qCritical() << "Editor is not a QComboBox" << editor;
    }
}

void ComboBoxItemDelegate::updateEditorGeometry(QWidget *editor,
                                                const QStyleOptionViewItem &option,
                                                const QModelIndex &index) const
{
    Q_UNUSED(index)

    editor->setGeometry(option.rect);
}
