#pragma once

#include <QStyledItemDelegate>

class ComboBoxItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT
    public:
        ComboBoxItemDelegate(QObject *parent = nullptr);

        void setIconSize(const QSize &size) { _iconSize = size; }

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const override;

        virtual void setEditorData(QWidget *editor,
                                   const QModelIndex &index) const override;

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const override;

        virtual void updateEditorGeometry(QWidget *editor,
                                          const QStyleOptionViewItem &option,
                                          const QModelIndex &index) const override;

        template <typename EnumClass, typename Enum>
        void fillFromEnum();

    private:
        typedef struct
        {
            QString text;
            QVariant data;
            QPixmap icon;
        } ComboBoxEntry;

    private:
        QList<ComboBoxEntry> _comboBoxData;
        QSize _iconSize;
};

#include <QDebug>
template<typename EnumClass, typename Enum>
void ComboBoxItemDelegate::fillFromEnum()
{
    _comboBoxData.clear();

    for(Enum value : EnumClass::getValues())
    {
        _comboBoxData.append(ComboBoxEntry{EnumClass::toTranslatedString(value),
                                           value,
                                           EnumClass::icon(value)});
    }
}
