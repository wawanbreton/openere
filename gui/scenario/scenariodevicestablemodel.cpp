#include "scenariodevicestablemodel.h"

#include <QDebug>

#include "commands/generic/setobjectpropertycommand.h"
#include "models/device.h"
#include "models/scenario.h"


ScenarioDevicesTableModel::ScenarioDevicesTableModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

void ScenarioDevicesTableModel::setScenario(const Scenario *scenario)
{
    if(_scenario)
    {
        disconnect(_scenario, 0, this, 0);
    }

    _scenario = scenario;
    #warning clear model

    if(_scenario)
    {
        connect(_scenario, &Scenario::deviceAboutToBeAdded,
                this,      &ScenarioDevicesTableModel::onDeviceAboutToBeAdded);
        connect(_scenario, &Scenario::deviceAdded,
                this,      &ScenarioDevicesTableModel::onDeviceAdded);
        connect(_scenario, &Scenario::deviceAboutToBeRemoved,
                this,      &ScenarioDevicesTableModel::onDeviceAboutToBeRemoved);
        connect(_scenario, &Scenario::deviceRemoved,
                this,      &ScenarioDevicesTableModel::onDeviceRemoved);

        for(const Device *device : _scenario->getDevices())
        {
            onDeviceAboutToBeAdded();
            onDeviceAdded(device);
        }
    }
}

int ScenarioDevicesTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return _scenario ? _scenario->getDevices().count() : 0;
}

QVariant ScenarioDevicesTableModel::data(const QModelIndex &index, int role) const
{
    if(index.isValid() &&
       _scenario && index.row() >= 0 &&
       index.row() < _scenario->getDevices().count())
    {
        const Device *device = _scenario->getDevices().at(index.row());

        if(index.column() == ColumnType)
        {
            if(role == Qt::DisplayRole)
            {
                return DeviceType::toTranslatedString(device->getType());
            }
            else if(role == Qt::EditRole)
            {
                return device->getType();
            }
            else if(role == Qt::DecorationRole)
            {
                return DeviceType::icon(device->getType()).scaled(_iconSize,
                                                                  Qt::KeepAspectRatio,
                                                                  Qt::SmoothTransformation);
            }
        }
        else if(index.column() == ColumnName)
        {
            if(role == Qt::DisplayRole || role == Qt::EditRole)
            {
                return device->getName();
            }
        }
    }

    return QVariant();
}

bool ScenarioDevicesTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.isValid() &&
       _scenario &&
       index.row() >= 0 &&
       index.row() < _scenario->getDevices().count() &&
       role == Qt::EditRole)
    {
        Device *device = _scenario->getDevices().at(index.row());
        if(index.column() == ColumnType)
        {
            emit processCommand(new SetObjectPropertyCommand(device, "type", value));
        }
        else if(index.column() == ColumnName)
        {
            emit processCommand(new SetObjectPropertyCommand(device, "name", value));
        }
        return true;
    }

    return false;
}

Qt::ItemFlags ScenarioDevicesTableModel::flags(const QModelIndex &index) const
{
    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

QVariant ScenarioDevicesTableModel::headerData(int section,
                                               Qt::Orientation orientation,
                                               int role) const
{
    if(orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        switch(Column(section))
        {
            case ColumnType:
                return tr("Type");
            case ColumnName:
                return tr("Nom");
        }
    }

    return QAbstractTableModel::headerData(section, orientation, role);
}

void ScenarioDevicesTableModel::onDeviceAboutToBeAdded()
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
}

void ScenarioDevicesTableModel::onDeviceAdded(const Device *device)
{
    endInsertRows();

    connect(device, &Device::typeChanged, this, [this]() { onDeviceChanged(ColumnType); });
    connect(device, &Device::nameChanged, this, [this]() { onDeviceChanged(ColumnName); });
}

void ScenarioDevicesTableModel::onDeviceAboutToBeRemoved(Device *device)
{
    QModelIndex index = makeIndex(device, 0);
    if(index.isValid())
    {
        beginRemoveRows(QModelIndex(), index.row(), index.row());
    }
}

void ScenarioDevicesTableModel::onDeviceRemoved()
{
    endRemoveRows();
}

void ScenarioDevicesTableModel::onDeviceChanged(ScenarioDevicesTableModel::Column dataColumn)
{
    if(_scenario)
    {
        Device *device = qobject_cast<Device *>(sender());
        if(device)
        {
            QModelIndex index = makeIndex(device, dataColumn);
            if(index.isValid())
            {
                emit dataChanged(index, index);
            }
        }
        else
        {
            qCritical() << "Sender is not a Device" << sender();
        }
    }
}

QModelIndex ScenarioDevicesTableModel::makeIndex(Device *device, int column)
{
    int row = _scenario->getDevices().indexOf(device);
    if(row >= 0)
    {
        return createIndex(row, column);
    }
    else
    {
        qCritical() << "Device not found" << device;
        return QModelIndex();
    }
}
