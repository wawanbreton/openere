#pragma once

#include <QAbstractTableModel>

#include <QSize>
#include <QUndoCommand>

class Device;
class Scenario;

class ScenarioDevicesTableModel : public QAbstractTableModel
{
    Q_OBJECT

    public:
        ScenarioDevicesTableModel(QObject *parent = nullptr);

        void setIconSize(const QSize &size) { _iconSize = size; }

        void setScenario(const Scenario *scenario);

        virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;

        virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override
        { Q_UNUSED(parent); return 2; }

        virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;

        virtual Qt::ItemFlags flags(const QModelIndex &index) const override;

        virtual QVariant headerData(int section,
                                    Qt::Orientation orientation,
                                    int role = Qt::DisplayRole) const override;

    signals:
        void processCommand(QUndoCommand *command);

    private:
        typedef enum
        {
            ColumnType = 0,
            ColumnName = 1
        } Column;

    private:
        void onDeviceAboutToBeAdded();

        void onDeviceAdded(const Device *device);

        void onDeviceAboutToBeRemoved(Device *device);

        void onDeviceRemoved();

        void onDeviceChanged(Column dataColumn);

        QModelIndex makeIndex(Device *device, int column);

    private:
        const Scenario *_scenario{nullptr};
        QSize _iconSize;
};
