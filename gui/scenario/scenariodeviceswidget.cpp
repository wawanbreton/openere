#include "scenariodeviceswidget.h"
#include "ui_scenariodeviceswidget.h"

#include <QDebug>

#include "commands/scenario/addscenariodevicecommand.h"
#include "commands/scenario/removescenariodevicecommand.h"
#include "gui/delegates/comboboxitemdelegate.h"
#include "models/device.h"
#include "models/scenario.h"
#include "scenariodevicestablemodel.h"


ScenarioDevicesWidget::ScenarioDevicesWidget(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::ScenarioDevicesWidget),
    _modelDevices(new ScenarioDevicesTableModel(this))
{
    _ui->setupUi(this);

    _ui->toolButtonAddDevice->setDefaultAction(_ui->actionAddDevice);
    _ui->toolButtonDeleteDevice->setDefaultAction(_ui->actionDeleteDevice);

    const QSize deviceTypeIconSize(24, 24);
    _ui->listDevices->verticalHeader()->setDefaultSectionSize(30);

    _modelDevices->setIconSize(deviceTypeIconSize);
    _ui->listDevices->setModel(_modelDevices);

    ComboBoxItemDelegate *delegateDeviceType = new ComboBoxItemDelegate(this);
    delegateDeviceType->fillFromEnum<DeviceType, DeviceType::Enum>();
    delegateDeviceType->setIconSize(deviceTypeIconSize);
    _ui->listDevices->setItemDelegateForColumn(0, delegateDeviceType);

    connect(_ui->actionAddDevice, &QAction::triggered,
            this, [this]() { emit processCommand(new AddScenarioDeviceCommand(_scenario)); } );
    connect(_ui->actionDeleteDevice, &QAction::triggered,
            this,                    &ScenarioDevicesWidget::onDeleteDeviceTriggered );
    connect(_modelDevices, &ScenarioDevicesTableModel::processCommand,
            this,          &ScenarioDevicesWidget::processCommand);
    connect(_ui->listDevices->selectionModel(), &QItemSelectionModel::selectionChanged,
            this,                               &ScenarioDevicesWidget::onSelectionChanged);

    _ui->actionDeleteDevice->setEnabled(false);
}

ScenarioDevicesWidget::~ScenarioDevicesWidget()
{
    delete _ui;
}

void ScenarioDevicesWidget::setScenario(Scenario *scenario)
{
    _scenario = scenario;
    _modelDevices->setScenario(scenario);
}

void ScenarioDevicesWidget::onDeleteDeviceTriggered()
{
    if(_scenario)
    {
        QModelIndexList selection = _ui->listDevices->selectionModel()->selectedRows();
        if(!selection.isEmpty())
        {
            int row = selection.first().row();
            if(row >= 0 && row < _scenario->getDevices().count())
            {
                Device *device = _scenario->getDevices().at(row);
                emit processCommand(new RemoveScenarioDeviceCommand(_scenario, device));
            }
        }
    }
}

void ScenarioDevicesWidget::onSelectionChanged(const QItemSelection &selected)
{
    _ui->actionDeleteDevice->setEnabled(!selected.isEmpty());
}
