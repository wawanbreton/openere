#pragma once

#include <QWidget>

#include <QUndoCommand>
#include <QItemSelection>

namespace Ui { class ScenarioDevicesWidget; }

class Device;
class Scenario;
class ScenarioDevicesTableModel;

class ScenarioDevicesWidget : public QWidget
{
    Q_OBJECT

    public:
        explicit ScenarioDevicesWidget(QWidget *parent = nullptr);
        ~ScenarioDevicesWidget();

        void setScenario(Scenario *scenario);

    signals:
        void processCommand(QUndoCommand *command);

    private:
        void onDeleteDeviceTriggered();

        void onSelectionChanged(const QItemSelection &selected);

    private:
        Ui::ScenarioDevicesWidget *_ui;
        ScenarioDevicesTableModel *_modelDevices{nullptr};
        Scenario *_scenario{nullptr};
};
