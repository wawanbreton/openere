#include "scenarioeditorwidget.h"
#include "ui_scenarioeditorwidget.h"

#include "models/scenario.h"
#include "commands/scenario/abstractscenarioeditorcommand.h"


ScenarioEditorWidget::ScenarioEditorWidget(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::ScenarioEditorWidget),
    _commandsStack(new QUndoStack(this)),
    _scenario(new Scenario(this))
{
    _ui->setupUi(this);

    _scenario->load(0);

    QAction *undoAction = _commandsStack->createUndoAction(this, tr("Annuler"));
    undoAction->setShortcut(Qt::CTRL + Qt::Key_Z);
    _ui->menuEdit->addAction(undoAction);

    QAction *redoAction = _commandsStack->createRedoAction(this, tr("Refaire"));
    redoAction->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_Z);
    _ui->menuEdit->addAction(redoAction);

    connect(_ui->tabDevices, &ScenarioDevicesWidget::processCommand,
            this,            &ScenarioEditorWidget::onProcessCommand);
    _ui->tabDevices->setScenario(_scenario);

    connect(_ui->tabMedia, &ScenarioMediaWidget::processCommand,
            this,          &ScenarioEditorWidget::onProcessCommand);
    _ui->tabMedia->setScenario(_scenario);

    connect(_ui->actionSave, &QAction::triggered,
            this,            &ScenarioEditorWidget::onSaveActionTriggered);
}

ScenarioEditorWidget::~ScenarioEditorWidget()
{
    delete _ui;
}

void ScenarioEditorWidget::onProcessCommand(QUndoCommand *command)
{
    _commandsStack->push(command);
}

void ScenarioEditorWidget::onSaveActionTriggered()
{
    _scenario->save(0);
}
