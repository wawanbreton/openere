#pragma once

#include <QMainWindow>

#include <QUndoStack>

namespace Ui { class ScenarioEditorWidget; }

class AbstractScenarioEditorCommand;
class Scenario;

class ScenarioEditorWidget : public QMainWindow
{
    Q_OBJECT

    public:
        explicit ScenarioEditorWidget(QWidget *parent = nullptr);
        ~ScenarioEditorWidget();

    private:
        void onProcessCommand(QUndoCommand *command);

        void onSaveActionTriggered();

    private:
        Ui::ScenarioEditorWidget *_ui;
        QUndoStack *_commandsStack{nullptr};
        Scenario *_scenario{nullptr};
};
