#include "scenariomediatablemodel.h"

#include <QDebug>

#include "commands/generic/setobjectpropertycommand.h"
#include "models/medium.h"
#include "models/scenario.h"


ScenarioMediaTableModel::ScenarioMediaTableModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

void ScenarioMediaTableModel::setScenario(const Scenario *scenario)
{
    if(_scenario)
    {
        disconnect(_scenario, 0, this, 0);
    }

    _scenario = scenario;

    if(_scenario)
    {
        connect(_scenario, &Scenario::mediumAboutToBeAdded,
                this,      &ScenarioMediaTableModel::onMediumAboutToBeAdded);
        connect(_scenario, &Scenario::mediumAdded,
                this,      &ScenarioMediaTableModel::onMediumAdded);
        connect(_scenario, &Scenario::mediumAboutToBeRemoved,
                this,      &ScenarioMediaTableModel::onMediumAboutToBeRemoved);
        connect(_scenario, &Scenario::mediumRemoved,
                this,      &ScenarioMediaTableModel::onMediumRemoved);
    }
}

int ScenarioMediaTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return _scenario ? _scenario->getMedia().count() : 0;
}

QVariant ScenarioMediaTableModel::data(const QModelIndex &index, int role) const
{
    if(index.isValid() &&
       _scenario && index.row() >= 0 &&
       index.row() < _scenario->getMedia().count())
    {
        const Medium *medium = _scenario->getMedia().at(index.row());

        if(index.column() == ColumnType)
        {
            if(role == Qt::DisplayRole)
            {
                return MediumType::toTranslatedString(medium->getType());
            }
            else if(role == Qt::EditRole)
            {
                return medium->getType();
            }
            else if(role == Qt::DecorationRole)
            {
                return MediumType::icon(medium->getType()).scaled(_iconSize,
                                                                  Qt::KeepAspectRatio,
                                                                  Qt::SmoothTransformation);
            }
        }
        else if(index.column() == ColumnName)
        {
            if(role == Qt::DisplayRole || role == Qt::EditRole)
            {
                return medium->getName();
            }
        }
    }

    return QVariant();
}

bool ScenarioMediaTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.isValid() &&
       _scenario &&
       index.row() >= 0 &&
       index.row() < _scenario->getMedia().count() &&
       role == Qt::EditRole)
    {
        Medium *medium = _scenario->getMedia().at(index.row());
        if(index.column() == ColumnType)
        {
            emit processCommand(new SetObjectPropertyCommand(medium, "type", value));
        }
        else if(index.column() == ColumnName)
        {
            emit processCommand(new SetObjectPropertyCommand(medium, "name", value));
        }
        return true;
    }

    return false;
}

Qt::ItemFlags ScenarioMediaTableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);

    if(index.column() == ColumnName)
    {
        flags |= Qt::ItemIsEditable;
    }

    return flags;
}

QVariant ScenarioMediaTableModel::headerData(int section,
                                               Qt::Orientation orientation,
                                               int role) const
{
    if(orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        switch(Column(section))
        {
            case ColumnType:
                return tr("Type");
            case ColumnName:
                return tr("Nom");
        }
    }

    return QAbstractTableModel::headerData(section, orientation, role);
}

void ScenarioMediaTableModel::onMediumAboutToBeAdded()
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
}

void ScenarioMediaTableModel::onMediumAdded(Medium *medium)
{
    endInsertRows();

    connect(medium, &Medium::typeChanged, this, [this]() { onMediumChanged(ColumnType); });
    connect(medium, &Medium::nameChanged, this, [this]() { onMediumChanged(ColumnName); });
}

void ScenarioMediaTableModel::onMediumAboutToBeRemoved(Medium *medium)
{
    QModelIndex index = makeIndex(medium, 0);
    if(index.isValid())
    {
        beginRemoveRows(QModelIndex(), index.row(), index.row());
    }
}

void ScenarioMediaTableModel::onMediumRemoved()
{
    endRemoveRows();
}

void ScenarioMediaTableModel::onMediumChanged(ScenarioMediaTableModel::Column dataColumn)
{
    if(_scenario)
    {
        Medium *medium = qobject_cast<Medium *>(sender());
        if(medium)
        {
            QModelIndex index = makeIndex(medium, dataColumn);
            if(index.isValid())
            {
                emit dataChanged(index, index);
            }
        }
        else
        {
            qCritical() << "Sender is not a Medium" << sender();
        }
    }
}

QModelIndex ScenarioMediaTableModel::makeIndex(Medium *medium, int column)
{
    int row = _scenario->getMedia().indexOf(medium);
    if(row >= 0)
    {
        return createIndex(row, column);
    }
    else
    {
        qCritical() << "Medium not found" << medium;
        return QModelIndex();
    }
}
