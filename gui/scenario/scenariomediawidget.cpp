#include "scenariomediawidget.h"
#include "ui_scenariomediawidget.h"

#include <QDebug>
#include <QFileDialog>
#include <QCamera>
#include <QMediaRecorder>
#include <QMediaPlayer>
#include <QMimeDatabase>
#include <QMimeType>

#include "commands/scenario/addscenariomediumcommand.h"
#include "commands/scenario/removescenariomediumcommand.h"
#include "models/device.h"
#include "models/scenario.h"
#include "scenariomediatablemodel.h"


ScenarioMediaWidget::ScenarioMediaWidget(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::ScenarioMediaWidget),
    _modelMedia(new ScenarioMediaTableModel(this))
{
    _ui->setupUi(this);

    _ui->toolButtonAddDevice->setDefaultAction(_ui->actionAddMedium);
    _ui->toolButtonDeleteDevice->setDefaultAction(_ui->actionDeleteMedium);

    const QSize mediumTypeIconSize(24, 24);
    _ui->listDevices->verticalHeader()->setDefaultSectionSize(30);

    _modelMedia->setIconSize(mediumTypeIconSize);
    _ui->listDevices->setModel(_modelMedia);

    connect(_ui->actionAddMedium, &QAction::triggered,
            this,                 &ScenarioMediaWidget::onAddMediumTriggered);
    connect(_ui->actionDeleteMedium, &QAction::triggered,
            this,                    &ScenarioMediaWidget::onDeleteMediumTriggered );
    connect(_modelMedia, &ScenarioMediaTableModel::processCommand,
            this,          &ScenarioMediaWidget::processCommand);
    connect(_ui->listDevices->selectionModel(), &QItemSelectionModel::selectionChanged,
            this,                               &ScenarioMediaWidget::onSelectionChanged);

    _ui->actionDeleteMedium->setEnabled(false);
}

ScenarioMediaWidget::~ScenarioMediaWidget()
{
    delete _ui;
}

void ScenarioMediaWidget::setScenario(Scenario *scenario)
{
    _scenario = scenario;
    _modelMedia->setScenario(scenario);
}

void ScenarioMediaWidget::onAddMediumTriggered()
{
    if(_imageSuffixes.isEmpty() && _videoSuffixes.isEmpty() && _audioSuffixes.isEmpty())
    {
        QCamera dummyCamera;
        QMediaRecorder dummyRecorder(&dummyCamera);
        QMimeDatabase mimeDb;
        for(const QString &mime : dummyRecorder.supportedVideoCodecs() +
                                  dummyRecorder.supportedAudioCodecs() +
                                  dummyRecorder.supportedContainers())
        {
            QMimeType fullType = mimeDb.mimeTypeForName(mime);
            QStringList mimeNameParts = fullType.name().split('/');
            if(mimeNameParts.count() == 2)
            {
                QString type = mimeNameParts.first();
                if(type == "image")
                {
                    _imageSuffixes.append(fullType.suffixes());
                }
                else if(type == "video")
                {
                    _videoSuffixes.append(fullType.suffixes());
                }
                else if(type == "audio")
                {
                    _audioSuffixes.append(fullType.suffixes());
                }
            }
        }
    }

    QFileDialog *dialog = new QFileDialog(this);
    dialog->setFileMode(QFileDialog::ExistingFiles);

    QStringList filters;
    filters << "Tous les fichiers (*)";
    filters << makeFileFilter("Images", _imageSuffixes);
    filters << makeFileFilter("Vidéos", _videoSuffixes);
    filters << makeFileFilter("Sons", _audioSuffixes);
    dialog->setNameFilters(filters);

    connect(dialog, &QFileDialog::accepted, this,   &ScenarioMediaWidget::onMediumFileSelected);
    connect(dialog, &QFileDialog::finished, dialog, &QFileDialog::deleteLater);

    dialog->show();

}

void ScenarioMediaWidget::onMediumFileSelected()
{
    QFileDialog *dialog = qobject_cast<QFileDialog *>(sender());
    if(dialog)
    {
        emit processCommand(new AddScenarioMediumCommand(dialog->selectedFiles(), _scenario));
    }
    else
    {
        qCritical() << "Sender is not a QFileDialog" << sender();
    }
}

void ScenarioMediaWidget::onDeleteMediumTriggered()
{
    if(_scenario)
    {
        QModelIndexList selection = _ui->listDevices->selectionModel()->selectedRows();
        if(!selection.isEmpty())
        {
            int row = selection.first().row();
            if(row >= 0 && row < _scenario->getMedia().count())
            {
                Medium *medium = _scenario->getMedia().at(row);
                emit processCommand(new RemoveScenarioMediumCommand(_scenario, medium));
            }
        }
    }
}

void ScenarioMediaWidget::onSelectionChanged(const QItemSelection &selected)
{
    _ui->actionDeleteMedium->setEnabled(!selected.isEmpty());
}

QString ScenarioMediaWidget::makeFileFilter(const QString &text, const QStringList &rawExtensions)
{
    QStringList fullExtensions;
    for(const QString &extension : rawExtensions)
    {
        fullExtensions << QString("*.%1").arg(extension);
    }

    return QString("%1 (%2)").arg(text).arg(fullExtensions.join(' '));
}
