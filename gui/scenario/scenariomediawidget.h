#pragma once

#include <QWidget>

#include <QUndoCommand>
#include <QItemSelection>

namespace Ui { class ScenarioMediaWidget; }

class Device;
class Scenario;
class ScenarioMediaTableModel;

class ScenarioMediaWidget : public QWidget
{
    Q_OBJECT

    public:
        explicit ScenarioMediaWidget(QWidget *parent = nullptr);
        ~ScenarioMediaWidget();

        void setScenario(Scenario *scenario);

    signals:
        void processCommand(QUndoCommand *command);

    private:
        void onAddMediumTriggered();

        void onMediumFileSelected();

        void onDeleteMediumTriggered();

        void onSelectionChanged(const QItemSelection &selected);

    private:
        static QString makeFileFilter(const QString &text, const QStringList &rawExtensions);

    private:
        inline static QStringList _imageSuffixes{};
        inline static QStringList _videoSuffixes{};
        inline static QStringList _audioSuffixes{};

    private:
        Ui::ScenarioMediaWidget *_ui;
        ScenarioMediaTableModel *_modelMedia{nullptr};
        Scenario *_scenario{nullptr};
};
