#include <QApplication>

#include "database/databaseaccessor.h"
#include "gui/scenario/scenarioeditorwidget.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("Open Escape Room Engine");

    DatabaseAccessor::init();

    ScenarioEditorWidget widget;
    widget.show();

    return app.exec();
}
