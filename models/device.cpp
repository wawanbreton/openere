#include "device.h"


Device::Device(QObject *parent) :
    QObject(parent)
{
}

void Device::setType(DeviceType::Enum type)
{
    if(type != _type)
    {
        _type = type;
        emit typeChanged(_type);
    }
}

void Device::setName(const QString &name)
{
    if(name != _name)
    {
        _name = name;
        emit nameChanged(_name);
    }
}
