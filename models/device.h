#pragma once

#include <QObject>

#include "models/devicetype.h"

class Device : public QObject
{
    Q_OBJECT
    Q_PROPERTY(DeviceType::Enum type READ getType WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)

    public:
        explicit Device(QObject *parent = nullptr);

        DeviceType::Enum getType() const { return _type; }

        void setType(DeviceType::Enum type);

        const QString &getName() const { return _name; }

        void setName(const QString &name);

    signals:
        void typeChanged(DeviceType::Enum type);

        void nameChanged(const QString &name);

    private:
        DeviceType::Enum _type{DeviceType::Input};
        QString _name;
};
