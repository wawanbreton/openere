#pragma once

#include "utils/enum.h"

#include <QPixmap>

class DeviceType : public QObject
{
    Q_OBJECT
    Q_ENUMS(Enum)

    public:
        typedef enum
        {
            Input,
            Output,
            Screen,
            Speaker
        } Enum;

        static QString toTranslatedString(Enum value)
        {
            switch(value)
            {
                case Input:
                    return tr("Entrée");
                case Output:
                    return tr("Sortie");
                case Screen:
                    return tr("Écran");
                case Speaker:
                    return tr("Haut-parleur");
            }

            return QString();
        }

        static QPixmap icon(Enum value)
        {
            switch(value)
            {
                case Input:
                    return QPixmap(":/assets/switch.svg");
                case Output:
                    return QPixmap(":/assets/light.svg");
                case Screen:
                    return QPixmap(":/assets/screen.svg");
                case Speaker:
                    return QPixmap(":/assets/speaker.svg");
            }

            return QPixmap();
        }

    LIST_ENUM(DeviceType)
    PRINT_ENUM(DeviceType)
    PARSE_ENUM(DeviceType)
};

Q_DECLARE_METATYPE(DeviceType::Enum)
