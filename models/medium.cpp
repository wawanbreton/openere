#include "medium.h"


Medium::Medium(QObject *parent) :
    QObject(parent)
{
}

void Medium::setType(MediumType::Enum type)
{
    if(type != _type)
    {
        _type = type;
        emit typeChanged(_type);
    }
}

void Medium::setName(const QString &name)
{
    if(name != _name)
    {
        _name = name;
        emit nameChanged(_name);
    }
}
