#pragma once

#include <QObject>

#include "models/mediumtype.h"

class Medium : public QObject
{
    Q_OBJECT
    Q_PROPERTY(MediumType::Enum type READ getType WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)

    public:
        explicit Medium(QObject *parent = nullptr);

        MediumType::Enum getType() const { return _type; }

        void setType(MediumType::Enum type);

        const QString &getName() const { return _name; }

        void setName(const QString &name);

    signals:
        void typeChanged(MediumType::Enum type);

        void nameChanged(const QString &name);

    private:
        MediumType::Enum _type{MediumType::Sound};
        QString _name;
        QIODevice *_data{nullptr};
};
