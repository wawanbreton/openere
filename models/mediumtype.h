#pragma once

#include "utils/enum.h"

#include <QPixmap>

class MediumType : public QObject
{
    Q_OBJECT
    Q_ENUMS(Enum)

    public:
        typedef enum
        {
            Sound,
            Video,
            Picture,
            Application,
        } Enum;

        static QString toTranslatedString(Enum value)
        {
            switch(value)
            {
                case Sound:
                    return tr("Son");
                case Video:
                    return tr("Vidéo");
                case Picture:
                    return tr("Image");
                case Application:
                    return tr("Application");
            }

            return QString();
        }

        static QPixmap icon(Enum value)
        {
            switch(value)
            {
                case Sound:
                    return QPixmap(":/assets/speaker.svg");
                case Video:
                    return QPixmap(":/assets/video.svg");
                case Picture:
                    return QPixmap(":/assets/picture.svg");
                case Application:
                    return QPixmap(":/assets/application.svg");
            }

            return QPixmap();
        }

    LIST_ENUM(MediumType)
};

Q_DECLARE_METATYPE(MediumType::Enum)
