#include "scenario.h"

#include <QDomDocument>
#include <QSqlError>
#include <QSqlQuery>

#include "database/databaseaccessor.h"
#include "models/device.h"
#include "models/medium.h"
#include "utils/debug.h"
#include "utils/xml.h"


Scenario::Scenario(QObject *parent) : QObject(parent)
{

}

void Scenario::load(int versionId)
{
    QSqlQuery queryData(DatabaseAccessor::db());
    queryData.prepare("SELECT data FROM scenarii_versions WHERE id=:id");
    queryData.bindValue(":id", versionId);

    if(queryData.exec())
    {
        if(queryData.next())
        {
            QDomDocument doc;
            QString errorMsg;
            int errorLine;
            int errorColumn;
            if(doc.setContent(queryData.value(0).toString(), &errorMsg, &errorLine, &errorColumn))
            {
                QDomNodeList scenarioNodes = doc.childNodes();
                for(int i = 0 ; i < scenarioNodes.count() ; ++i)
                {
                    QDomNode scenarioNode = scenarioNodes.at(i);
                    if(scenarioNode.nodeName() == "scenario")
                    {
                        QDomNodeList subNodes = scenarioNode.childNodes();
                        for(int j = 0 ; j < subNodes.count() ; ++j)
                        {
                            QDomNode subNode = subNodes.at(j);
                            if(subNode.nodeName() == "device")
                            {
                                Device *device = new Device();
                                device->setType(Xml::findEnumAttr<DeviceType, DeviceType::Enum>(subNode, "type", __METHOD__));
                                device->setName(Xml::findStringAttr(subNode, "name", __METHOD__));
                                _devices.append(device);
                            }
                        }
                    }
                }
            }
            else
            {
                qWarning() << "Error when parsing XML data" << errorMsg
                           << "at line" << errorLine
                           << "column" << errorColumn;
            }
        }
        else
        {
            qWarning() << "No data for scenario version" << versionId;
        }
    }
    else
    {
        qWarning() << "Query error" << queryData.lastError().text();
    }
}

void Scenario::save(int versionId)
{
    QDomDocument doc;

    QDomElement scenarioNode = doc.createElement("scenario");

    for(const Device *device : _devices)
    {
        QDomElement deviceNode = doc.createElement("device");
        Xml::storeEnumAttr<DeviceType>(deviceNode, "type", device->getType());
        Xml::storeStringAttr(deviceNode, "name", device->getName());
        scenarioNode.appendChild(deviceNode);
    }

    doc.appendChild(scenarioNode);

    QSqlQuery queryData(DatabaseAccessor::db());
    queryData.prepare("UPDATE scenarii_versions SET data=:data WHERE id=:id;");
    queryData.bindValue(":data", QString::fromUtf8(doc.toByteArray(2)));
    queryData.bindValue(":id", versionId);

    queryData.exec();
}

void Scenario::addDevice(Device *device, int index)
{
    if(index < 0 || index >= _devices.count())
    {
        index = _devices.count();
    }

    emit deviceAboutToBeAdded(device, index);

    _devices.insert(index, device);

    device->setParent(this);

    emit deviceAdded(device, index);
}

void Scenario::removeDevice(Device *device)
{
    int index = _devices.indexOf(device);
    if(index >= 0)
    {
        emit deviceAboutToBeRemoved(device, index);

        _devices.removeAt(index);

        device->setParent(nullptr);

        emit deviceRemoved(device, index);
    }
    else
    {
        qWarning() << "Device not registered";
    }
}

void Scenario::addMedium(Medium *medium, int index)
{
    if(index < 0 || index >= _media.count())
    {
        index = _media.count();
    }

    emit mediumAboutToBeAdded(medium, index);

    _media.insert(index, medium);

    medium->setParent(this);

    emit mediumAdded(medium, index);
}

void Scenario::removeMedium(Medium *medium)
{
    int index = _media.indexOf(medium);
    if(index >= 0)
    {
        emit mediumAboutToBeRemoved(medium, index);

        _media.removeAt(index);

        medium->setParent(nullptr);

        emit mediumRemoved(medium, index);
    }
    else
    {
        qWarning() << "Medium not registered";
    }
}
