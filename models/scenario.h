#pragma once

#include <QObject>

class Device;
class Medium;

class Scenario : public QObject
{
    Q_OBJECT

    public:
        explicit Scenario(QObject *parent = nullptr);

        void load(int versionId);

        void save(int versionId);

        void addDevice(Device *device, int index = -1);

        void removeDevice(Device *device);

        const QList<Device *> &getDevices() const { return _devices; }

        void addMedium(Medium *medium, int index = -1);

        void removeMedium(Medium *medium);

        const QList<Medium *> &getMedia() const { return _media; }

    signals:
        void deviceAboutToBeAdded(Device *device, int index);
        void deviceAdded(Device *device, int index);

        void deviceAboutToBeRemoved(Device *device, int index);
        void deviceRemoved(Device *device, int index);

        void mediumAboutToBeAdded(Medium *medium, int index);
        void mediumAdded(Medium *medium, int index);

        void mediumAboutToBeRemoved(Medium *medium, int index);
        void mediumRemoved(Medium *medium, int index);

    private:
        QList<Device *> _devices;
        QList<Medium *> _media;
};
