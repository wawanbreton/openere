#pragma once

#include <QDebug>
#include <QString>

#define __METHOD__ Debug::shortMethodName(__PRETTY_FUNCTION__)

/*! @brief Util class for debug */
class Debug
{
    public:
        inline static QString toHex(quint8 value)
        { return QString("0x%1").arg(value, 2, 16, QLatin1Char('0')); }

        inline static QString toHex(quint16 value)
        { return QString("0x%1").arg(value, 4, 16, QLatin1Char('0')); }

        inline static QString toHex(quint32 value)
        { return QString("0x%1").arg(value, 8, 16, QLatin1Char('0')); }

        inline static QString toHex(quint64 value)
        { return QString("0x%1").arg(value, 16, 16, QLatin1Char('0')); }

        inline static QString toHex(const QByteArray &data)
        { return "0x" + data.toHex(); }

        static QString shortMethodName(const QString &function);
};
