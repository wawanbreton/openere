#include "parser.h"

#include <QBuffer>
#include <QRegExp>
#ifdef QT_GUI_LIB
# include <QImage>
#endif


#define PARSE(TYPE, FULLTYPE) \
FULLTYPE Parser::parse##TYPE(const QString &text, \
                             const QString &debugCaller, \
                             const FULLTYPE &defaultValue) \
{ \
    FULLTYPE result = defaultValue; \
\
    bool ok = false; \
    FULLTYPE value = parse##TYPE(text, &ok); \
\
    if(ok) \
    { \
        result = value; \
    } \
    else if(not debugCaller.isEmpty()) \
    { \
        qWarning() << qPrintable(debugCaller) << "Could not convert" << text << "to a " #TYPE; \
    } \
\
    return result; \
}

#define PARSENULLABLE(TYPE, FULLTYPE, PARSEMETHOD) \
FULLTYPE Parser::parse##TYPE(const QString &text, bool *ok) \
{ \
    if( text == "null" )  /* NOLINT */ \
    { \
        *ok = true; \
        return FULLTYPE(); \
    } \
    else  /* NOLINT */ \
    { \
        FULLTYPE parsedValue = PARSEMETHOD; \
        if( not parsedValue.isNull() )  /* NOLINT */ \
        { \
            *ok = true; \
            return parsedValue; \
        } \
        else  /* NOLINT */ \
        { \
            *ok = false; \
            return FULLTYPE(); \
        } \
    } \
}

#define TOSTRINGNULLABLE(TYPE, FULLTYPE, TOSTRINGMETHOD) \
QString Parser::toString##TYPE(const FULLTYPE &value) \
{ \
    return value.isNull() ? "null" : TOSTRINGMETHOD; \
}

PARSE(UInt8, quint8)

PARSE(Int8, qint8)

qint8 Parser::parseInt8(const QString &text, bool *ok)
{
    qint32 result = parseInt32(text, ok);
    *ok &= result <= 127;
    *ok &= result > -128;
    return (qint8)result;
}

PARSE(UInt16, quint16)

PARSE(UInt32, quint32)

PARSE(UInt64, quint64)

PARSE(Int32, qint32)

PARSE(Int64, qint64)

PARSE(Bool, bool)

PARSE(Date, QDate)
PARSENULLABLE(Date, QDate, (QDate::fromString(text, "yyyy'/'MM'/'dd")))
TOSTRINGNULLABLE(Date, QDate, value.toString("yyyy'/'MM'/'dd"))

PARSE(Time, QTime)
PARSENULLABLE(Time, QTime, (QTime::fromString(text, "hh':'mm':'ss'.'zzz")))
TOSTRINGNULLABLE(Time, QTime, value.toString("hh':'mm':'ss'.'zzz"))

PARSE(DateTime, QDateTime)

QDateTime Parser::parseDateTime(const QString &text, bool *ok)
{
    if(text == "null")
    {
        *ok = true;
        return QDateTime();
    }
    else
    {
        QDateTime parsedValue = QDateTime::fromString(text, "yyyy'/'MM'/'dd' 'HH':'mm':'ss'.'zzz");
        if(not parsedValue.isNull())
        {
            *ok = true;
            parsedValue.setTimeSpec(Qt::UTC);
            return parsedValue.toLocalTime();
        }
        else
        {
            *ok = false;
            return QDateTime();
        }
    }
}

TOSTRINGNULLABLE(DateTime, QDateTime,
                 value.toUTC().toString("yyyy'/'MM'/'dd' 'HH':'mm':'ss'.'zzz"))

PARSE(Uuid, QUuid)
PARSENULLABLE(Uuid, QUuid, QUuid(text))
TOSTRINGNULLABLE(Uuid, QUuid, value.toString())

#ifdef QT_GUI_LIB
PARSE(Image, QImage)

PARSENULLABLE(Image, QImage, (QImage::fromData(QByteArray::fromBase64(text.toUtf8()), "PNG")))

QString Parser::toStringImage(const QImage &value)
{
    if(value.isNull())
    {
        return "null";
    }
    else
    {
        QBuffer buffer;
        buffer.open(QIODevice::WriteOnly);
        value.save(&buffer, "PNG");
        return buffer.data().toBase64();
    }
}
#endif

#ifdef QT_NETWORK_LIB
PARSE(Address, QHostAddress)

QHostAddress Parser::parseAddress(const QString &text, bool *ok)
{
    if(text == "null")
    {
        *ok = true;
        return QHostAddress();
    }
    else
    {
        // Don't use QHostAddress standard parsing because it interprets leading zeros as octal
        // value, which is standard, but unexpected for humans beings...
        QRegExp regExp("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
                       "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
                       "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
                       "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
        if(regExp.exactMatch(text) && regExp.captureCount() == 4)
        {
            quint32 ipv4 = 0;
            ipv4 |= regExp.capturedTexts()[1].toUInt() << 24;
            ipv4 |= regExp.capturedTexts()[2].toUInt() << 16;
            ipv4 |= regExp.capturedTexts()[3].toUInt() << 8;
            ipv4 |= regExp.capturedTexts()[4].toUInt() << 0;

            QHostAddress parsedValue(ipv4);
            *ok = true;
            return parsedValue;
        }
        else
        {
            *ok = false;
            return QHostAddress();
        }
    }
}

TOSTRINGNULLABLE(Address, QHostAddress, value.toString())
#endif

QVariant Parser::parse(const QString &text,
                       const QVariant &defaultValue,
                       const QString &debugCaller)
{
    switch(defaultValue.type())
    {
        case QVariant::Bool:
            return parseBool(text, debugCaller, defaultValue.toBool());
        case QVariant::Int:
            return parseInt32(text, debugCaller, defaultValue.toInt());
        case QVariant::LongLong:
            return parseInt64(text, debugCaller, defaultValue.toLongLong());
        case QVariant::String:
            return text;
        default:
            break;
    }

    qCritical() << "Parsing method for type" << defaultValue.type() << "not implemented";
    return QVariant();
}

QString Parser::toString(const QVariant &value)
{
    switch(value.type())
    {
        case QVariant::Bool:
            return toStringBool(value.toBool());
        case QVariant::Int:
            return toStringInt32(value.toInt());
        case QVariant::LongLong:
            return toStringInt64(value.toLongLong());
        case QVariant::String:
            return value.toString();
        default:
            break;
    }

    qCritical() << "toString method for type" << value.type() << "not implemented";
    return QString();
}
