#pragma once

#include <QDate>
#ifdef QT_NETWORK_LIB
#include <QHostAddress>
#endif
#ifdef QT_GUI_LIB
#include <QColor>
#include <QImage>
#endif
#include <QMetaEnum>
#include <QStringList>
#include <QUuid>
#include <QVariant>

#include "utils/enum.h"

class Parser
{
    public:
        static quint8 parseUInt8(const QString &text,
                                 const QString &debugCaller = "",
                                 const quint8 &defaultValue = 0);

        inline static quint8 parseUInt8(const QString &text, bool *ok)
        { quint16 result = parseUInt16(text, ok); *ok &= result <= 0x00ff; return (quint8)result; }

        inline static QString toStringUInt8(const quint8 &value)
        { return QString::number(value); }

        static qint8 parseInt8(const QString &text,
                               const QString &debugCaller = "",
                               const qint8 &defaultValue = 0);

        static qint8 parseInt8(const QString &text, bool *ok);

        inline static QString toStringInt8(const qint8 &value)
        { return QString::number(value); }

        static quint16 parseUInt16(const QString &text,
                                   const QString &debugCaller = "",
                                   const quint16 &defaultValue = 0);

        inline static quint16 parseUInt16(const QString &text, bool *ok)
        { return text.startsWith("0x") ? text.mid(2).toUShort(ok, 16) : text.toUShort(ok); }

        inline static QString toStringUInt16(const quint16 &value)
        { return QString::number(value); }

        static quint32 parseUInt32(const QString &text,
                                   const QString &debugCaller = "",
                                   const quint32 &defaultValue = 0);

        inline static quint32 parseUInt32(const QString &text, bool *ok)
        { return text.startsWith("0x") ? text.mid(2).toUInt(ok, 16) : text.toUInt(ok); }

        inline static QString toStringUInt32(const quint32 &value)
        { return QString::number(value); }

        static quint64 parseUInt64(const QString &text,
                                   const QString &debugCaller = "",
                                   const quint64 &defaultValue = 0);

        inline static quint64 parseUInt64(const QString &text, bool *ok)
        { return text.startsWith("0x") ? text.mid(2).toULongLong(ok, 16) : text.toULongLong(ok); }

        inline static QString toStringUInt64(const quint64 &value)
        { return QString::number(value); }

        static qint32 parseInt32(const QString &text,
                                 const QString &debugCaller = "",
                                 const qint32 &defaultValue = 0);

        inline static qint32 parseInt32(const QString &text, bool *ok)
        { return text.toInt(ok); }

        inline static QString toStringInt32(const qint32 &value)
        { return QString::number(value); }

        static qint64 parseInt64(const QString &text,
                                 const QString &debugCaller = "",
                                 const qint64 &defaultValue = 0);

        inline static qint64 parseInt64(const QString &text, bool *ok)
        { return text.toLongLong(ok); }

        inline static QString toStringInt64(const qint64 &value)
        { return QString::number(value); }

        static bool parseBool(const QString &text,
                              const QString &debugCaller = "",
                              const bool &defaultValue = false);

        inline static bool parseBool(const QString &text, bool *ok)
        { QVariant var(text); *ok = var.canConvert(QVariant::Bool); return var.toBool(); }

        inline static QString toStringBool(const bool &value)
        { return value ? "true" : "false"; }

        static QDate parseDate(const QString &text,
                               const QString &debugCaller = "",
                               const QDate &defaultValue = QDate());

        static QDate parseDate(const QString &text, bool *ok);

        static QString toStringDate(const QDate &value);

        static QTime parseTime(const QString &text,
                               const QString &debugCaller = "",
                               const QTime &defaultValue = QTime());

        static QTime parseTime(const QString &text, bool *ok);

        static QString toStringTime(const QTime &value);

        static QDateTime parseDateTime(const QString &text,
                                       const QString &debugCaller = "",
                                       const QDateTime &defaultValue = QDateTime());

        static QDateTime parseDateTime(const QString &text, bool *ok);

        static QString toStringDateTime(const QDateTime &value);

        template <typename EnumClass, typename Enum>
        static Enum parseEnum(const QString &text,
                              Enum defaultValue,
                              const QString &debugCaller);

        template <typename EnumClass, typename Enum>
        static Enum parseEnum(const QString &text, const QString &debugCaller = "");

        template <typename EnumClass, typename Enum>
        static Enum parseEnum(const QString &text, bool *ok);

        template <typename EnumClass, typename Enum>
        static QString toStringEnum(Enum value);

        template <typename EnumClass, typename Enum>
        static QList<Enum> parseEnumList(const QString &text, const QString &debugCaller);

        template <typename EnumClass, typename Enum>
        static QString toStringEnumList(const QList<Enum> &values);

        static QUuid parseUuid(const QString &text,
                               const QString &debugCaller = "",
                               const QUuid &defaultValue = QUuid());

        static QUuid parseUuid(const QString &text, bool *ok);

        static QString toStringUuid(const QUuid &value);

        #ifdef QT_GUI_LIB
        static QImage parseImage(const QString &text,
                                 const QString &debugCaller = "",
                                 const QImage &defaultValue = QImage());

        static QImage parseImage(const QString &text, bool *ok);

        static QString toStringImage(const QImage &value);

        inline static QString toStringColor(const QColor &value)
        { return value.isValid() ? value.name() : "null"; }
        #endif

        #ifdef QT_NETWORK_LIB
        static QHostAddress parseAddress(const QString &text,
                                         const QString &debugCaller = "",
                                         const QHostAddress &defaultValue = QHostAddress());

        static QHostAddress parseAddress(const QString &text, bool *ok);

        static QString toStringAddress(const QHostAddress &value);
        #endif

        static QVariant parse(const QString &text,
                              const QVariant &defaultValue,
                              const QString &debugCaller = QString());

        static QString toString(const QVariant &value);
};

template <typename EnumClass, typename Enum>
Enum Parser::parseEnum(const QString &text,
                       Enum defaultValue,
                       const QString &debugCaller)
{
    Enum result = defaultValue;

    bool ok;
    Enum value = parseEnum<EnumClass, Enum>(text, &ok);

    if(ok)
    {
        result = value;
    }
    else if(not debugCaller.isEmpty())
    {
        qWarning() << qPrintable(debugCaller) << "Could not convert" << text
                   << "to" << EnumClass::staticMetaObject.className();
    }

    return result;
}

template <typename EnumClass, typename EnumType>
EnumType Parser::parseEnum(const QString &text, const QString &debugCaller)
{
    return Parser::parseEnum<EnumClass, EnumType>(text, Enum::defaultValue<EnumClass, EnumType>(),
                                                  debugCaller);
}

template <typename EnumClass, typename Enum>
Enum Parser::parseEnum(const QString &text, bool *ok)
{
    return EnumClass::fromString(text, ok);
}

template <typename EnumClass, typename Enum>
QString Parser::toStringEnum(Enum value)
{
    return EnumClass::toString(value);
}

template <typename EnumClass, typename Enum>
QList<Enum> Parser::parseEnumList(const QString &text, const QString &debugCaller)
{
    QList<Enum> result;

    foreach(const QString &enumText, text.split(';', QString::SkipEmptyParts))
    {
        bool ok;
        Enum value = parseEnum<EnumClass, Enum>(enumText, &ok);
        if(ok)
        {
            result << value;
        }
        else
        {
            qWarning() << qPrintable(debugCaller) << "Could not convert" << enumText
                       << "to" << EnumClass::staticMetaObject.className();
        }
    }

    return result;
}

template <typename EnumClass, typename Enum>
QString Parser::toStringEnumList(const QList<Enum> &values)
{
    QStringList texts;

    foreach(Enum value, values)
    {
        texts << toStringEnum<EnumClass, Enum>(value);
    }

    return texts.join(";");
}
