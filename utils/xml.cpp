#include "xml.h"

#include <QBuffer>
#include <QFile>


#define FINDATTR(TYPE, FULLTYPE) \
FULLTYPE Xml::find##TYPE##Attr(const QDomNode &node, const QString &attrName, \
                                const QString &debugCaller, const FULLTYPE &defaultValue, \
                                bool warnIfNotFound) \
{ \
    QString attrString; \
    if(readStringAttr(node, attrName, debugCaller, attrString, warnIfNotFound)) \
    { \
        return Parser::parse##TYPE(attrString, debugCaller, defaultValue); \
    } \
    \
    return defaultValue; \
}

#define STOREATTR(TYPE, FULLTYPE) \
void Xml::store##TYPE##Attr(QDomElement &node, const QString &attrName, const FULLTYPE &value) \
{ \
    storeStringAttr(node, attrName, Parser::toString##TYPE(value)); \
}

#define STOREVALUE(TYPE, FULLTYPE) \
void Xml::store##TYPE##Value(QDomNode &node, \
                             QDomDocument &document, \
                             const QString &valueName, \
                             const FULLTYPE &value) \
{ \
    storeStringValue(node, document, valueName, Parser::toString##TYPE(value)); \
}


QString Xml::findStringAttr(const QDomNode &node, const QString &attrName,
                            const QString &debugCaller, const QString &defaultValue,
                            bool warnIfNotFound)
{
    QString result = defaultValue;

    readStringAttr(node, attrName, debugCaller, result, warnIfNotFound);

    return result;
}

FINDATTR(UInt8, quint8)

FINDATTR(Int8, qint8)

FINDATTR(UInt16, quint16)

FINDATTR(UInt32, quint32)

FINDATTR(Int32, qint32)

FINDATTR(UInt64, quint64)

FINDATTR(Int64, qint64)

FINDATTR(Bool, bool)

FINDATTR(DateTime, QDateTime)

FINDATTR(Date, QDate)

FINDATTR(Time, QTime)

FINDATTR(Uuid, QUuid)

#ifdef QT_GUI_LIB
FINDATTR(Image, QImage)
#endif

#ifdef QT_NETWORK_LIB
FINDATTR(Address, QHostAddress)
#endif

void Xml::storeStringAttr(QDomElement &node, const QString &attrName, const QString &value)
{
    node.setAttribute(attrName, value);
}

void Xml::storeStringValue(QDomNode &node,
                           QDomDocument &document,
                           const QString &valueName,
                           const QString &value)
{
    QDomElement subNode = document.createElement(valueName);
    subNode.appendChild(document.createTextNode(value));
    node.appendChild(subNode);
}

STOREATTR(Bool, bool)

STOREATTR(UInt8, quint8)

STOREATTR(UInt16, quint16)

STOREVALUE(UInt16, quint16)

STOREATTR(UInt32, quint32)

STOREATTR(UInt64, quint64)

STOREATTR(Int8, qint8)

STOREATTR(Int32, qint32)

STOREATTR(Int64, qint64)

STOREATTR(DateTime, QDateTime)

STOREATTR(Date, QDate)

STOREATTR(Time, QTime)

STOREATTR(Uuid, QUuid)

#ifdef QT_NETWORK_LIB
STOREATTR(Address, QHostAddress)
#endif

#ifdef QT_GUI_LIB
STOREATTR(Image, QImage)

STOREATTR(Color, QColor)
#endif


#define FINDVALUE(TYPE, FULLTYPE) \
FULLTYPE Xml::find##TYPE##ChildNodeValue(const QDomNode &node, const QString &nodeName, \
                                const QString &debugCaller, const FULLTYPE &defaultValue) \
{ \
    QString nodeValue = findChildNodeValue(node, nodeName, true); \
    if(!nodeValue.isEmpty()) \
    { \
        return Parser::parse##TYPE(nodeValue, debugCaller, defaultValue); \
    } \
    \
    return defaultValue; \
}

FINDVALUE(Bool, bool)

FINDVALUE(UInt8, quint8)

FINDVALUE(UInt16, quint16)

FINDVALUE(Int32, qint32)

QDomDocument Xml::readFileDoc(const QString &filePath)
{
    QDomDocument result;

    QFile file(filePath);
    if(file.exists())
    {
        if(file.open(QIODevice::ReadOnly))
        {
            result = readDeviceDoc(&file);
        }
        else
        {
            qWarning() << "Unable to open file" << filePath << ":" << file.errorString();
        }
    }
    else
    {
        qWarning() << "The file" << filePath << "does not exist";
    }

    return result;
}

QDomDocument Xml::readDeviceDoc(QIODevice *device)
{
    QDomDocument doc;
    QString errorMsg;
    int errorLine, errorColumn;
    if(not doc.setContent(device, false, &errorMsg, &errorLine, &errorColumn))
    {
        qWarning() << "Invalid XML file at line" << errorLine
                   << "column" << errorColumn << ":" << errorMsg;
    }

    return doc;
}

QDomNode Xml::readFile(const QString &filePath, bool warnNotFound)
{
    QString error;
    if(not warnNotFound && not QFile::exists(filePath))
    {
        return QDomNode();
    }

    QDomNode result = readFile(filePath);

    if(not result.isNull())
    {
        qWarning() << "Error when loading file" << filePath << ":" << error;
    }

    return result;
}

QDomNode Xml::readDevice(QIODevice *device)
{
    QDomDocument doc = readDeviceDoc(device);
    QDomNodeList childNodes = doc.childNodes();
    for(int i = 0 ; i < childNodes.count() ; i++)
    {
        QDomNode childNode = childNodes.at(i);
        if(childNode.nodeType() == QDomNode::ElementNode)  // Skip the comments or descriptors
        {
            return childNode;
        }
    }

    return QDomNode();
}

QDomNode Xml::findChildNode(const QDomNode &parent, const QString &childName)
{
    QList<QDomNode> result;

    result = findChildNodes(parent, childName, 1);

    if( result.isEmpty() )
        return QDomNode();
    else
        return result.first();
}

QList<QDomNode> Xml::findChildNodes(const QDomNode &parent, const QString &childName, int maxCount)
{
    QList<QDomNode> nodeList;
    QDomNodeList children = parent.childNodes();
    bool b_maxNodeCountReached = false;

    for(int i = 0 ; (i < children.count()) && (b_maxNodeCountReached == false); i++)
    {
        QDomNode child = children.at(i);
        if(childName.isEmpty() || child.nodeName() == childName)
        {
            nodeList << child;
            if(maxCount > 0)
            {
                b_maxNodeCountReached = ( nodeList.count() >= maxCount );
            }
        }
    }

    return nodeList;
}

QString Xml::findChildNodeValue(const QDomNode &parent, const QString &childName, bool trimmed)
{
    QString nodeText;

    nodeText = findChildNode(parent, childName).firstChild().nodeValue();
    if(trimmed && not nodeText.isNull())
    {
        nodeText = nodeText.trimmed();
    }

    return nodeText;
}

QString Xml::findStringChildNodeValue(const QDomNode &node, const QString &nodeName,
                                      const QString &defaultValue)
{
    QList<QDomNode> result = findChildNodes(node, nodeName, 1);
    if(!result.isEmpty())
    {
        if(!result.first().firstChild().isNull())
        {
            return result.first().firstChild().nodeValue();
        }
    }

    return defaultValue;
}

bool Xml::readStringAttr(const QDomNode &node, const QString &attrName,
                         const QString &debugCaller, QString &value, bool warnIfNotFound)
{
    if(node.isElement())
    {
        QDomElement element = node.toElement();
        if(element.hasAttribute(attrName))
        {
            value = element.attribute(attrName);
            return true;
        }
       else if(warnIfNotFound)
       {
           qWarning() << qPrintable(debugCaller) << "Could not find the required XML attribute"
                      << attrName << "on node" << node.nodeName();
       }
    }
    else
    {
        qWarning() << qPrintable(debugCaller) << "The given XML node" << node.nodeName()
                   << "is not an element";
    }

    return false;
}


bool Xml::hasAttribute(const QDomNode &parent, const QString &attrName)
{
    if(parent.isElement())
    {
        QDomElement element = parent.toElement();
        return element.hasAttribute(attrName);
    }

    return false;
}
