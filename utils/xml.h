#pragma once

#include <QDateTime>
#include <QDomNode>
#ifdef QT_NETWORK_LIB
#include <QHostAddress>
#endif
#ifdef QT_GUI_LIB
#include <QColor>
#include <QImage>
#endif
#include <QMetaEnum>
#include <QUuid>

#include "utils/enum.h"
#include "utils/parser.h"

class Xml
{
    public:
        static quint8 findUInt8Attr(const QDomNode &node, const QString &attrName,
                                    const QString &debugCaller, const quint8 &defaultValue = 0,
                                    bool warnIfNotFound = true);

        static qint8 findInt8Attr(const QDomNode &node, const QString &attrName,
                                  const QString &debugCaller, const qint8 &defaultValue = 0,
                                  bool warnIfNotFound = true);

        static quint8 findUInt8ChildNodeValue(const QDomNode &node, const QString &nodeName,
                                        const QString &debugCaller, const quint8 &defaultValue = 0);

        static quint16 findUInt16Attr(const QDomNode &node, const QString &attrName,
                                      const QString &debugCaller, const quint16 &defaultValue = 0,
                                      bool warnIfNotFound = true);

        static quint16 findUInt16ChildNodeValue(const QDomNode &node, const QString &nodeName,
                                      const QString &debugCaller, const quint16 &defaultValue = 0);

        static quint32 findUInt32Attr(const QDomNode &node, const QString &attrName,
                                      const QString &debugCaller, const quint32 &defaultValue = 0,
                                      bool warnIfNotFound = true);

        static quint64 findUInt64Attr(const QDomNode &node, const QString &attrName,
                                      const QString &debugCaller, const quint64 &defaultValue = 0,
                                      bool warnIfNotFound = true);

        static qint64 findInt64Attr(const QDomNode &node, const QString &attrName,
                                    const QString &debugCaller, const qint64 &defaultValue = 0,
                                    bool warnIfNotFound = true);

        static qint32 findInt32Attr(const QDomNode &node, const QString &attrName,
                                    const QString &debugCaller, const qint32 &defaultValue = 0,
                                    bool warnIfNotFound = true);

        static qint32 findInt32ChildNodeValue(const QDomNode &node, const QString &nodeName,
                                      const QString &debugCaller, const qint32 &defaultValue = 0);

        static bool findBoolAttr(const QDomNode &node, const QString &attrName,
                                 const QString &debugCaller, const bool &defaultValue = false,
                                 bool warnIfNotFound = true);

        static bool findBoolChildNodeValue(const QDomNode &node, const QString &nodeName,
                                      const QString &debugCaller, const bool &defaultValue = false);

        static QString findStringAttr(const QDomNode &node, const QString &attrName,
                                      const QString &debugCaller = "", const QString &defaultValue = "",
                                      bool warnIfNotFound = true);

        static QString findStringChildNodeValue(const QDomNode &node, const QString &nodeName,
                                                const QString &defaultValue = "");

        static QDateTime findDateTimeAttr(const QDomNode &node, const QString &elementName,
                                          const QString &debugCaller,
                                          const QDateTime &defaultValue = QDateTime(),
                                          bool warnIfNotFound = true);

        static QDate findDateAttr(const QDomNode &node, const QString &elementName,
                                  const QString &debugCaller, const QDate &defaultValue = QDate(),
                                  bool warnIfNotFound = true);

        static QTime findTimeAttr(const QDomNode &node, const QString &elementName,
                                  const QString &debugCaller,
                                  const QTime &defaultValue = QTime(),
                                  bool warnIfNotFound = true);

        template <typename EnumClass, typename Enum>
        static Enum findEnumAttr(const QDomNode &node, const QString &attrName,
                                 const QString &debugCaller, Enum defaultValue,
                                 bool warnIfNotFound = true);

        template <typename EnumClass, typename Enum>
        static Enum findEnumAttr(const QDomNode &node, const QString &attrName,
                                 const QString &debugCaller, bool warnIfNotFound = true);

        template <typename EnumClass, typename Enum>
        static Enum findEnumChildNodeValue(const QDomNode &node, const QString &childName,
                                           const QString &debugCaller, Enum defaultValue);

        template <typename EnumClass, typename Enum>
        static QList<Enum> findEnumListAttr(const QDomNode &node, const QString &attrName,
                                            const QString &debugCaller,
                                            const QList<Enum> &defaultValue = QList<Enum>(),
                                            bool warnIfNotFound = true);

        static QUuid findUuidAttr(const QDomNode &node, const QString &attrName,
                                  const QString &debugCaller, const QUuid &defaultValue = QUuid(),
                                  bool warnIfNotFound = true);

        #ifdef QT_NETWORK_LIB
        static QHostAddress findAddressAttr(const QDomNode &node,
                                            const QString &attrName,
                                            const QString &debugCaller,
                                            const QHostAddress &defaultValue = QHostAddress(),
                                            bool warnIfNotFound = true);
        #endif

        #ifdef QT_GUI_LIB
        static QImage findImageAttr(const QDomNode &node,
                                    const QString &attrName,
                                    const QString &debugCaller,
                                    const QImage &defaultValue = QImage(),
                                    bool warnIfNotFound = true);
        #endif

        static void storeUInt8Attr(QDomElement &node, const QString &attrName, const quint8 &value);

        static void storeUInt16Attr(QDomElement &node, const QString &attrName,
                                    const quint16 &value);

        static void storeUInt16Value(QDomNode &node,
                                     QDomDocument &document,
                                     const QString &valueName,
                                     const quint16 &value);

        static void storeUInt32Attr(QDomElement &node, const QString &attrName,
                                    const quint32 &value);

        static void storeUInt64Attr(QDomElement &node, const QString &attrName,
                                    const quint64 &value);

        static void storeInt8Attr(QDomElement &node, const QString &attrName, const qint8 &value);

        static void storeInt32Attr(QDomElement &node, const QString &attrName, const qint32 &value);

        static void storeInt64Attr(QDomElement &node, const QString &attrName, const qint64 &value);

        static void storeBoolAttr(QDomElement &node, const QString &attrName, const bool &value);

        static void storeStringAttr(QDomElement &node, const QString &attrName,
                                    const QString &value);

        static void storeStringValue(QDomNode &node,
                                     QDomDocument &document,
                                     const QString &valueName,
                                     const QString &value);

        static void storeDateTimeAttr(QDomElement &node, const QString &attrName,
                                      const QDateTime &value);

        static void storeDateAttr(QDomElement &node, const QString &attrName, const QDate &value);

        static void storeTimeAttr(QDomElement &node, const QString &attrName, const QTime &value);

        template <typename EnumClass, typename Enum>
        static void storeEnumAttr(QDomElement &node, const QString &attrName, Enum value);

        template <typename EnumClass, typename Enum>
        static void storeEnumListAttr(QDomElement &node,
                                      const QString &attrName,
                                      const QList<Enum> &values);

        static void storeUuidAttr(QDomElement &node, const QString &attrName, const QUuid &value);

        #ifdef QT_NETWORK_LIB
        static void storeAddressAttr(QDomElement &node, const QString &attrName,
                                     const QHostAddress &address);
        #endif

        #ifdef QT_GUI_LIB
        static void storeImageAttr(QDomElement &node, const QString &attrName, const QImage &image);

        static void storeColorAttr(QDomElement &node, const QString &attrName, const QColor &color);
        #endif

        static bool saveDocumentToFile(const QDomDocument &doc,
                                       const QString &fileName,
                                       bool verySafe = false);

        inline static QByteArray saveDocument(const QDomDocument &doc)
        { return doc.toByteArray(2); }

        static QDomDocument readFileDoc(const QString &filePath);

        static QDomDocument readDeviceDoc(QIODevice *device);

        inline static QDomNode readFile(const QString &filePath)
        { return readFileDoc(filePath).firstChildElement(); }

        static QDomNode readFile(const QString &filePath, bool warnNotFound);

        static QDomNode readDevice(QIODevice *device);

        static QDomNode findChildNode(const QDomNode &parent, const QString &childName);

        static QList<QDomNode> findChildNodes(const QDomNode &parent,
                                              const QString &childName = QString(),
                                              int maxCount = 0);

        static bool hasAttribute(const QDomNode &parent, const QString &attrName);

        static QString findChildNodeValue(const QDomNode &parent, const QString &childName,
                                          bool trimmed = true);

    private:
        static bool readStringAttr(const QDomNode &node, const QString &attrName,
                                   const QString &debugCaller, QString &value,
                                   bool warnIfNotFound = true);
};

// Template functions have to be defined in the header
template <typename EnumClass, typename Enum>
Enum Xml::findEnumAttr(const QDomNode &node, const QString &attrName,
                       const QString &debugCaller, Enum defaultValue, bool warnIfNotFound)
{
    QString attrString;
    if(readStringAttr(node, attrName, debugCaller, attrString, warnIfNotFound))
    {
        return Parser::parseEnum<EnumClass, Enum>(attrString, defaultValue, debugCaller);
    }

    return defaultValue;
}

template <typename EnumClass, typename EnumType>
EnumType Xml::findEnumAttr(const QDomNode &node, const QString &attrName,
                           const QString &debugCaller, bool warnIfNotFound)
{
    return findEnumAttr<EnumClass>(node, attrName, debugCaller,
                                   Enum::defaultValue<EnumClass, EnumType>(), warnIfNotFound);
}

template <typename EnumClass, typename Enum>
Enum Xml::findEnumChildNodeValue(const QDomNode &node, const QString &childName,
                                 const QString &debugCaller, Enum defaultValue)
{
    QString enumString = findChildNodeValue(node, childName, true);
    if(!enumString.isEmpty())
    {
        return Parser::parseEnum<EnumClass, Enum>(enumString, defaultValue, debugCaller);
    }

    return defaultValue;
}

template <typename EnumClass, typename Enum>
QList<Enum> Xml::findEnumListAttr(const QDomNode &node, const QString &attrName,
                                  const QString &debugCaller, const QList<Enum> &defaultValue,
                                  bool warnIfNotFound)
{
    QString attrString;
    if(readStringAttr(node, attrName, debugCaller, attrString, warnIfNotFound))
    {
        return Parser::parseEnumList<EnumClass, Enum>(attrString, debugCaller);
    }

    return defaultValue;
}

template <typename EnumClass, typename Enum>
void Xml::storeEnumAttr(QDomElement &node, const QString &attrName, Enum value)
{
    storeStringAttr(node, attrName, Parser::toStringEnum<EnumClass, Enum>(value));
}

template <typename EnumClass, typename Enum>
void Xml::storeEnumListAttr(QDomElement &node, const QString &attrName, const QList<Enum> &values)
{
    storeStringAttr(node, attrName, Parser::toStringEnumList<EnumClass, Enum>(values));
}

Q_DECLARE_METATYPE(QDomDocument)
